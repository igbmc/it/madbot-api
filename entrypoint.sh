madbot collectstatic --noinput
madbot makemigrations core
madbot migrate
# create oauth client if env settings defined
gunicorn madbot_api.wsgi --bind 0.0.0.0:8000 --timeout 420
