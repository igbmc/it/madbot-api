import logging
from urllib.parse import urlparse

import omero
from Glacier2 import PermissionDeniedException
from Ice import DNSException
from omero.gateway import BlitzGateway

from madbot_api.core.connector import (
    AuthenticationError,
    ConnectorUnreachableError,
    DataObject,
    DataObjectNotFoundError,
    DataConnector,
    ToolConnector,
    LIST_STRUCTURE,
)

logger = logging.getLogger(__name__)


class OmeroConnector(DataConnector):
    def __init__(self, parameters):
        self.url = parameters["url"]
        self.login = parameters["login"]
        self.password = parameters["password"]

        # Test omero instance
        self.get_omero_instance()

    @classmethod
    def get_name(cls):
        return "Omero"

    @classmethod
    def get_description(cls):
        return "OMERO handles all your images using a secure central repository, from the microscope to publication."

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        return "madbot_omero/omero.png"

    @classmethod
    def get_color(cls):
        color = "#7CDD98"
        return color

    @classmethod
    def get_connection_param(cls):
        return [
            {
                "id": "url",
                "name": "Omero server URL",
                "helper": "Exemple :  https://omero.mesocentre.uca.fr/",
                "type": "text",
                "access": "public",
            },
            {
                "id": "login",
                "name": "Private login",
                "helper": "",
                "type": "text",
                "access": "private",
            },
            {
                "id": "password",
                "name": "Private password",
                "helper": "",
                "type": "password",
                "access": "private",
            },
        ]

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_port(self):
        return 4064

    def get_host(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return host

    def get_api_url(self):
        WEB_HOST = "v0/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        return url_api

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        WEB_DISPLAY = "/webclient/?show="
        url = self.url + WEB_DISPLAY + obj_type + "-" + str(obj_id)
        return url


    def get_instance_name(self):
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return str(host)

    def get_root_name(self):
        return "All project"

    def get_data_objects(self, parent_type, parent_id):
        client = self.get_omero_instance()
        datas = []

        if parent_id:
            if parent_type.lower() == "project":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                for data in conn.getObjects(
                    "Dataset", opts={"project": parent_id}
                ):
                    datas.append(
                        DataObject(
                            id=data.getId(),
                            name=data.getName(),
                            type="dataset",
                            description=data.getDescription(),
                            url=self.get_url_link_to_an_object("dataset", data.getId()),
                            has_children=True if data.countChildren() else False,
                            linkable=True,
                            icon="madbot_omero/dataset.png",
                        )
                    )
            elif parent_type.lower() == "dataset":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                for data in conn.getObjects(
                    "Image", opts={"dataset": parent_id}
                ):
                    datas.append(
                        DataObject(
                            id=data.getId(),
                            name=data.getName(),
                            type="image",
                            description=data.getDescription(),
                            url=self.get_url_link_to_an_object("image", data.getId()),
                            has_children=False,
                            linkable=True,
                            icon="madbot_omero/image.png",
                        )
                    )
            else:
                raise DataObjectNotFoundError("parent_type should either be 'project' or 'dataset'")

        else:
            conn = BlitzGateway(client_obj=client)
            conn.SERVICE_OPTS.setOmeroGroup("-1")
            for data in conn.listProjects():
                datas.append(
                    DataObject(
                        id=data.getId(),
                        name=data.getName(),
                        type="project",
                        description=data.getDescription(),
                        url=self.get_url_link_to_an_object("project", data.getId()),
                        has_children=True if data.countChildren() else False,
                        linkable=True,
                        icon="madbot_omero/project.png",
                    )
                )
        return datas

    def get_data_object(self, object_type, object_id):
        client = self.get_omero_instance()
        if object_id:
            if object_type.lower() == "project":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("project", object_id)
                if object is None:
                    raise DataObjectNotFoundError("The data object was not found")
                children = True if object.countChildren() else False
                icon = "madbot_omero/project.png"

            elif object_type.lower() == "dataset":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("dataset", object_id)
                if object is None:
                    raise DataObjectNotFoundError("The data object was not found")
                children = True if object.countChildren() else False
                icon = "madbot_omero/dataset.png"

            elif object_type.lower() == "image":
                conn = BlitzGateway(client_obj=client)
                conn.SERVICE_OPTS.setOmeroGroup("-1")
                object = conn.getObject("image", object_id)
                if object is None:
                    raise DataObjectNotFoundError("The data object was not found")
                children = False
                icon = "madbot_omero/image.png"
                
            else:
                raise DataObjectNotFoundError("object_type should either be 'project', 'dataset' or 'image'")

            data = DataObject(
                id=object.getId(),
                name=object.getName(),
                type=object_type,
                description=object.getDescription(),
                has_children=children,
                icon=icon,
                url=self.get_url_link_to_an_object(object_type, object.getId()),
            )
        else:
            raise DataObjectNotFoundError("data_object_id not provided")
        return data

    def get_omero_instance(self):
        client = omero.client(self.get_host(), self.get_port())
        try:
            client.createSession(str(self.login), str(self.password))
        except PermissionDeniedException:
            raise AuthenticationError(
                    "Invalid Omero login or password"
                )
        except omero.ClientError:
            raise ConnectorUnreachableError(
                    "Omero server URL (" + self.url + ") is not reachable"
                )
        except DNSException:
            raise ConnectorUnreachableError(
                    "Omero server URL (" + self.url + ") is not reachable"
                )
        return client



ToolConnector.register(OmeroConnector)
