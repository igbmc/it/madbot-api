from django.apps import AppConfig


class MadbotOmeroConfig(AppConfig):
    name = "madbot_api.connectors.madbot_omero"
