from django.apps import AppConfig


class MadbotLabGuruConfig(AppConfig):
    name = "madbot_api.connectors.madbot_labguru"
