import logging
import requests

from bs4 import BeautifulSoup
from urllib.parse import urlparse

from madbot_api.core.connector import (
    AuthenticationError,
    ConnectorUnreachableError,
    DataObject,
    DataObjectNotFoundError,
    DefaultError,
    ISAImporter,
    ToolConnector,
    LIST_STRUCTURE,
)

logger = logging.getLogger(__name__)


class LabGuruConnector(ISAImporter):
    def __init__(self, parameters):
        self.url = parameters["url"]
        self.private_token = parameters["private_token"]

        self.api_url = f"{self.url}/api/v1"

        # Test URL connection & Token

        test_url = f"{self.api_url}/admin/members.json?token={self.private_token}"
        try:
            test = requests.get(test_url)
            if test.status_code != requests.codes.ok:
                if test.status_code == 401:
                    raise AuthenticationError(
                        "Invalid Token for Labguru server URL (" + self.url + ")"
                    )
                else:
                    raise ConnectorUnreachableError(
                        "LabGuru server URL (" + self.url + ") is not reachable"
                        )
        except requests.exceptions.ConnectionError:
            raise ConnectorUnreachableError(
                "Labguru server URL (" + self.url + ") is not reachable"
            )

    #
    # Default fonction
    #
    @classmethod
    def get_name(cls):
        return "LabGuru"

    @classmethod
    def get_description(cls):
        return "Connect to a LabGuru electronic lab notebook"

    @classmethod
    def get_type(cls):
        return ["dataconnector", "isaimporter"]

    @classmethod
    def get_connection_param(cls):
        return [
            {
                "id": "url",
                "name": "LabGuru URL",
                "helper": "Exemple :  https://my.labguru.com",
                "type": "text",
                "access": "public",
            },
            {
                "id": "private_token",
                "name": "Private token",
                "helper": 'How to get a LabGuru token ?<br/>1. Connect to LabGuru<br/>2. Go to your LabGuru Profile<br/>3. Click on LabGuru\'s Uplfolder<br/>4. Click on the blue rectangle that says "Click here to get a token by email"<br/>5. You will receive your LabGuru token in your mailbox',
                "type": "text",
                "access": "private",
            },
        ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        return "madbot_labguru/labguru.png"

    @classmethod
    def get_color(cls):
        return "#F7F5FF"

    def get_instance_name(self):
        parsed_uri = urlparse(self.api_url)
        return parsed_uri.netloc

    def get_url_link_to_an_object(self, obj_type, obj_id):
        url=f"{self.url}/knowledge/{obj_type}/{obj_id}"
        return url

    def get_root_name(self):
        return "All projects"

    def get_data_objects(self, parent_type, parent_id):
        datas = []
        if parent_id is None:
            # retrieve project
            url = f"{self.api_url}/projects?token={self.private_token}"
            try:
                projects = requests.get(url)
            except requests.exceptions.ConnectionError:
                raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
            except requests.exceptions.HTTPError:
                raise DataObjectNotFoundError("expired Token")
            for project in projects.json():
                datas.append(
                    DataObject(
                        id = project["id"],
                        name = project["name"],
                        description = BeautifulSoup(
                            project["description"], features="html.parser")
                        .get_text(),
                        type = "project",
                        icon = "fas fa-briefcase",
                        url = self.get_url_link_to_an_object("projects", project["id"]),
                        has_children = True,
                        linkable = True,
                        children = [],
                    )
                )

        else:
            if parent_type == "project":
                # retrieve folders
                url = f"{self.api_url}/milestones?token={self.private_token}&meta=true&project_id={parent_id}"
                try:
                    folders = requests.get(url)
                except requests.exceptions.ConnectionError:
                    raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
                except requests.exceptions.HTTPError:
                    raise DataObjectNotFoundError("wrong parent_id or expired Token ")
                for folder in folders.json():
                    datas.append(
                        DataObject(
                        id = folder["id"],
                        name = folder["name"],
                        description = BeautifulSoup(folder["description"], features="html.parser").get_text(),
                        type = "folder",
                        icon = "fas fa-folder",
                        url = self.get_url_link_to_an_object("milestones", folder["id"]),
                        has_children = True,
                        linkable = True,
                        children = [],
                        )
                    )


            elif parent_type == "folder":
                # retrive experiments
                url = f'{self.api_url}/experiments?token={self.private_token}&filter={{"milestone_id":{parent_id}}}'
                try:
                    experiments = requests.get(url)
                except requests.exceptions.ConnectionError:
                    raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
                except requests.exceptions.HTTPError:
                    raise DataObjectNotFoundError("wrong parent_id or expired Token ")
                for experiment in experiments.json():
                    datas.append(
                        DataObject(
                            id = experiment["id"],
                            name = experiment["name"],
                            type = "experiment",
                            icon = "fas fa-flask",
                            url = self.get_url_link_to_an_object("experiments", experiment["id"]),
                            has_children = False,
                            linkable = True,
                            children = [],
                        )
                    )

            else:
                    raise DataObjectNotFoundError("data_object_type should either be 'project', 'folder' or 'experiment'")
        return sorted(datas, key=lambda data: data.name)

    def get_data_object(self, object_type, object_id, depth=0):
        if object_id:
            if object_type == "project":
                url = f"{self.api_url}/projects/{object_id}?token={self.private_token}"
                try:
                    project = requests.get(url).json()
                except requests.exceptions.ConnectionError:
                    raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
                except requests.exceptions.HTTPError:
                    raise DataObjectNotFoundError("wrong object_id or expired Token ")
                if "id" not in project:
                    raise DataObjectNotFoundError("The data object was not found")
                parent = DataObject(
                    id = project["id"],
                    name = project["name"],
                    description = BeautifulSoup(project["description"], features="html.parser").get_text(),
                    type = "project",
                    icon = "fas fa-briefcase",
                    url = self.get_url_link_to_an_object("project", project["id"]),
                    has_children = True,
                    linkable = True,
                    children = [],
                )

                if int(depth) > 0:
                    for child in self.get_data_objects("project", project["id"]):
                        parent.children.append(
                            self.get_data_object(child.type, child.id, int(depth) - 1)
                        )
            elif object_type == "folder":
                url = f"{self.api_url}/milestones/{object_id}?token={self.private_token}"
                try:
                    folder = requests.get(url).json()
                except requests.exceptions.ConnectionError:
                    raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
                except requests.exceptions.HTTPError:
                    raise DataObjectNotFoundError("wrong object_id or expired Token ")
                if "id" not in folder:
                    raise DataObjectNotFoundError("The data object was not found")
                parent = DataObject(
                    id = folder["id"],
                    name = folder["name"],
                    description = BeautifulSoup(folder["description"], features="html.parser").get_text(),
                    type = "folder",
                    icon = "fas fa-folder",
                    url = self.get_url_link_to_an_object("milestones", folder["id"]),
                    has_children = True,
                    linkable = True,
                    children = [],
                )

                if int(depth) > 0:
                    for child in self.get_data_objects("folder", folder["id"]):
                        parent.children.append(
                            self.get_data_object(child.type, child.id, int(depth) - 1)
                        )
            elif object_type == "experiment":
                url = f"{self.api_url}/experiments/{object_id}?token={self.private_token}"
                try:
                    experiment = requests.get(url).json()
                except requests.exceptions.ConnectionError:
                    raise ConnectorUnreachableError("LabGuru server URL (" + self.url + ") is not reachable")
                except requests.exceptions.HTTPError:
                    raise DataObjectNotFoundError("wrong object_id or expired Token ")
                if "id" not in experiment:
                    raise DataObjectNotFoundError("The data object was not found")
                parent = DataObject(
                    id = experiment["id"],
                    name = experiment["name"],
                    type = "experiment",
                    icon = "fa-flask",
                    url = self.get_url_link_to_an_object("experiments", experiment["id"]),
                    has_children = False,
                    linkable = True,
                    children = [],
                )

            else:
                raise DataObjectNotFoundError("data_object_type should either be 'project', 'folder' or 'experiment'")
        else:
            raise DataObjectNotFoundError("data_object_id not provided")
        return parent


ToolConnector.register(LabGuruConnector)
