import base64
import logging
import os
from urllib.parse import urlparse

import requests
from bioblend import ConnectionError, galaxy
from django.core.files import File
from rest_framework import status
from rest_framework.response import Response

from madbot_api.core.connector import (
    AuthenticationError,
    ConnectorUnreachableError,
    DataConnector,
    DataObject,
    DataObjectNotFoundError,
    DefaultError,
    ToolConnector,
    LIST_STRUCTURE,
)

logger = logging.getLogger(__name__)


class GalaxyConnector(DataConnector):
    def __init__(self, parameters):
        self.url = parameters["url"]
        self.login = parameters["login"]
        self.password = parameters["password"]

        # Test URL connection
        try:
            response = requests.get(self.url)
            if response.status_code != 200:
                raise ConnectorUnreachableError(
                    "Galaxy server URL (" + self.url + ") is not reachable"
                )
        except requests.exceptions.ConnectionError:
            raise ConnectorUnreachableError(
                "Galaxy server URL (" + self.url + ") is not reachable"
            )

        # Test User Login
        self.get_galaxy_instance()

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "Galaxy"

    @classmethod
    def get_description(cls):
        return "Galaxy is an open source, web-based platform for data intensive biomedical research."

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_connection_param(cls):
        return [
                {
                    "id": "url",
                    "name": "Galaxy server URL",
                    "helper": "Exemple :  https://usegalaxy.org",
                    "type": "text",
                    "access": "public",
                },
                {
                    "id": "login",
                    "name": "Private login",
                    "helper": "",
                    "type": "text",
                    "access": "private",
                },
                {
                    "id": "password",
                    "name": "Private password",
                    "helper": "",
                    "type": "password",
                    "access": "private",
                }
            ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_logo(cls):
        return "madbot_galaxy/galaxy.png"

    @classmethod
    def get_color(cls):
        color = "#7FAEE5"
        return color

    def get_instance_name(self):
        gi = galaxy.GalaxyInstance(
            url=self.url, email=self.login, password=self.password, verify=True
        )
        name = gi.config.get_config()["brand"]
        if name:
            return "Galaxy " + str(name)
        else:
            url_parse = urlparse(gi.base_url)
            host = url_parse.hostname
            return str(host)

    def get_url_link_to_an_object(self, object_type, object_id):
        if object_type == "history":
            url = os.path.join(self.url + f"/history/switch_to_history?hist_id={object_id}")
        elif object_type == "datasets":
            url = os.path.join(self.url + f"/datasets/{object_id}/details")
        return url

    def get_root_name(self):
        return "All histories"

    def get_data_objects(self, parent_type, parent_id):
        if parent_id:
            if parent_type == "history":
                datas = []
                gi = self.get_galaxy_instance()
                try:
                    di = gi.datasets.get_datasets(history_id=parent_id)
                except ConnectionError:
                    raise DataObjectNotFoundError("The data object provided 'parent_id' was not found")
                for d in di:
                    if not d["deleted"]:
                        try:
                            ds = gi.datasets.show_dataset(
                                dataset_id=str(d["id"])
                            )
                        except ConnectionError:
                            raise DataObjectNotFoundError("One of the datasets was not found.")
                        datas.append(
                            DataObject(
                                id=d["id"],
                                name=d["name"],
                                description=ds["misc_info"],
                                type="dataset",
                                url=self.get_url_link_to_an_object("datasets", d["id"]),
                                icon="fas fa-file",
                                has_children=False,
                                linkable=True,
                            )
                        )
                return datas
            else:
                raise DataObjectNotFoundError("data_object_type should be 'history'")
        else:
            gi = self.get_galaxy_instance()
            histories = []
            hi = gi.histories.get_histories()
            for h in hi:
                histories.append(
                    DataObject(
                        id=h["id"],
                        name=h["name"],
                        description=h["annotation"],
                        type="history",
                        icon="fas fa-clock-rotate-left",
                        url=self.get_url_link_to_an_object("history", h["id"]),
                        has_children=not gi.histories.show_history(
                            history_id=h["id"]
                        )["empty"],
                        linkable=True,
                    )
                )
            return histories


    def get_data_object(self, object_type, object_id, depth=0):
        gi = self.get_galaxy_instance()
        if object_id:
            has_children = False
            if object_type.lower() == "history":
                try:
                    object = gi.histories.show_history(history_id=object_id)
                except ConnectionError:
                    raise DataObjectNotFoundError("The data object was not found")
                description=object["annotation"]
                type = "history"
                icon = "fas fa-clock-rotate-left"
                url=self.get_url_link_to_an_object("history", object["id"])

                children=[]
                if int(depth) > 0:
                    history_children = self.get_data_objects('history', object['id'])
                    if history_children:
                        has_children=True
                        for child in history_children:
                            children.append(self.get_data_object(child.type, child.id, int(depth)-1))

            elif object_type.lower() == "dataset":
                try:
                    object = gi.datasets.show_dataset(dataset_id=object_id)
                except ConnectionError:
                    raise DataObjectNotFoundError("The data object was not found.")
                description=object["misc_info"]
                type = "data"
                icon = "file"
                url=self.get_url_link_to_an_object("datasets", object["id"])
                children = []

            else:
                raise DataObjectNotFoundError("data_object_type should either be 'history' or 'datasets'")


            if object["deleted"]:
                    raise DataObjectNotFoundError("The data object was not found")
            else :
                data = DataObject(
                    id=object["id"],
                    name=object["name"],
                    description=description,
                    type=type,
                    icon=icon,
                    url=url,
                    linkable=True,
                    has_children=has_children,
                    children=children,
                )
        else:
            raise DataObjectNotFoundError("data_object_id not provided")
        return data

    def get_galaxy_instance(self):
        try:
            gi = galaxy.GalaxyInstance(
                url=self.url,
                email=self.login,
                password=self.password,
                verify=True,
            )
            status_code = gi.make_get_request(url=gi.base_url).status_code
            if status_code != requests.codes.ok:
                if status_code == 401:
                    raise AuthenticationError("Invalid username or password")
                else:
                    raise ConnectorUnreachableError("Code return :" + str(status_code))
        except Exception as e:
            if str(e) == "Failed to authenticate user.":
                raise AuthenticationError("Invalid username or password.")
            else:
                raise ConnectorUnreachableError(str(e))
        return gi


ToolConnector.register(GalaxyConnector)
