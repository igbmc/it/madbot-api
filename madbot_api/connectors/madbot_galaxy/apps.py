from django.apps import AppConfig


class MadbotGalaxyConfig(AppConfig):
    name = "madbot_api.connectors.madbot_galaxy"
