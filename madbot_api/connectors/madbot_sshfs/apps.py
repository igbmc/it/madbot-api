from django.apps import AppConfig


class MadbotSSHFSConfig(AppConfig):
    name = "madbot_api.connectors.madbot_sshfs"
