import base64
import logging
import os.path
import socket
import stat

from paramiko.client import AutoAddPolicy, SSHClient
from paramiko.ssh_exception import (
    AuthenticationException,
    NoValidConnectionsError,
    SSHException,
)

from madbot_api.core.connector import (
    LIST_STRUCTURE,
    AuthenticationError,
    ConnectorUnreachableError,
    DataObjectNotFoundError,
    DefaultError,
    DataObject,
    DataConnector,
    ToolConnector,
)

logger = logging.getLogger(__name__)


class SSHFSConnector(DataConnector):
    def __init__(self, parameters):
        self.host = parameters["host"]
        self.username = parameters["username"]
        self.password = parameters["password"]

        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy)
        try:
            self.client.connect(
                hostname=self.host,
                username=self.username,
                password=self.password,
            )
        except AuthenticationException:
            raise AuthenticationError("Invalid username or password")
        except NoValidConnectionsError:
            raise AuthenticationError("Unable to establish connection to host")
        except SSHException:
            raise AuthenticationError("Unexpected SSH connection error")
        except socket.gaierror:
            raise ConnectorUnreachableError(
                    "Hostname (" + self.host + ") is not reachable"
                )
        except Exception as e:
            raise DefaultError(type(e).__name__ + ":" + str(e))


        self.sftp = self.client.open_sftp()

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "SSHFS"

    @classmethod
    def get_description(cls):
        return "Connect to any NAS or cluster through SSH"

    @classmethod
    def get_type(cls):
        return "dataconnector"

    @classmethod
    def get_connection_param(cls):
        return [
            {
                "id": "host",
                "name": "SSH host",
                "helper": "Exemple :  core.cluster.france-bioinformatique.fr",
                "type": "text",
                "access": "public",
            },
            {
                "id": "username",
                "name": "Username",
                "helper": "",
                "type": "text",
                "access": "private",
            },
            {
                "id": "password",
                "name": "Password",
                "helper": "",
                "type": "password",
                "access": "private",
            },
        ]

    @classmethod
    def get_data_structure(cls):
        return LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return False

    @classmethod
    def get_logo(cls):
        return "madbot_sshfs/sshfs.png"

    @classmethod
    def get_color(cls):
        return "#F5FEFF"

    def get_instance_name(self):
        return self.host

    def get_url_link_to_an_object(self, obj_type, obj_id):
        return "todo"

    def get_root_name(self):
        return "/"

    def get_data_objects(self, parent_type, parent_id):
        datas = []
        path = "/"

        if parent_id is not None:
            path = parent_id
        
        if parent_type != "file":
            try:
                for file_attr in self.sftp.listdir_iter(path):
                    is_dir = stat.S_ISDIR(file_attr.st_mode)

                    datas.append(DataObject(
                        id = os.path.join(path, file_attr.filename),
                        name = file_attr.filename,
                        type = "directory" if is_dir else "file",
                        icon = "fas fa-folder" if is_dir else "fas fa-file",
                        has_children = is_dir,
                        linkable = True
                    ))
            except FileNotFoundError:
                raise DataObjectNotFoundError(f"{parent_type} content not found")
        else:
            raise DataObjectNotFoundError(f"the {parent_type} {parent_id} is not a directory")

        return sorted(datas, key=lambda data: data.name)

    def get_data_object(self, object_type, object_id):
        try:
            object_stat = self.sftp.stat(object_id)
            if (stat.S_ISDIR(object_stat.st_mode) and object_type == "directory") or (stat.S_ISREG(object_stat.st_mode) and object_type == "file"):
                return DataObject(
                    id=object_id,
                    name=os.path.basename(object_id),
                    type=object_type,
                    icon="fas fa-folder" if object_type == "directory" else "fas fa-file",
                    has_children=object_type == "directory",
                    linkable=True,
                )
            else:
                raise DataObjectNotFoundError(f"{object_id} is not a {object_type}")
        except FileNotFoundError:
            raise DataObjectNotFoundError(f"{object_type} not found")



ToolConnector.register(SSHFSConnector)
