from django.urls import include, path
from django.contrib import admin
from rest_framework import routers
from madbot_api.core.url_api import urlpatterns as api_url_patterns

router = routers.DefaultRouter()
urlpatterns = [
    path("", include(router.urls)),

    path('api/', include(api_url_patterns)),
    path("admin/", admin.site.urls),
]
