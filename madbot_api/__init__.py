import os
import sys

__version__ = "1.0.1-dev.7"
VERSION = __version__


def manage():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "madbot_api.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
