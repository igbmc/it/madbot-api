from django.urls import include, path

from madbot_api.core.views import (
    tokens,
    investigations,
    studies,
    assays,
    connectors,
    tools,
    datalinks,
    members,
    users,
)
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView


assays_url_patterns = [
    # base-path = api/investigations/<iid>/studies/assays/
    path("<int:aid>/", assays.AssayView.as_view()),
]

studies_url_patterns = [
    # base-path = api/investigations/<iid>/studies/
    path("<int:sid>/", studies.StudyView.as_view()),
    path("<int:sid>/assays/", assays.AssaysListView.as_view()),
]

investigation_url_patterns = [
    # base-path = api/investigations/
    path("", investigations.InvestigationsListView.as_view()),
    path("<int:iid>/", investigations.InvestigationView.as_view()),
    path("<int:iid>/studies/", studies.StudiesListView.as_view()),
    # Members
    path("<int:iid>/members/",
         members.InvestigationMemberView.as_view(
             {'get': 'get_members_of_investigation',
              'post': 'create_member'})),
    path("<int:iid>/members/<str:user_identifier>/",
         members.InvestigationMemberView.as_view(
             {'get': 'get_member_role',
              'put': 'update_member_role',
              'delete': 'delete_member'})),
]

investigation_member_url_patterns = [
    # base-path = api/members/
    path("<str:user_identifier>/investigations/",
         members.InvestigationMemberView.as_view(
             {'get': 'get_investigation_of_member'})),
    path("description/roles", members.InvestigationMemberView.as_view(
        {'get': 'get_roles_information'})),
]

urlpatterns = [
    # base-path = api/
    path("tokens/", tokens.TokensView.as_view()),
    path("user/", tokens.UserAPI.as_view()),
    path("logout/", tokens.LogoutAPI.as_view()),
    path("investigations/", include(investigation_url_patterns)),
    path("studies/", include(studies_url_patterns)),
    path("assays/", include(assays_url_patterns)),
    path("members/", include(investigation_member_url_patterns)),
    path("users/", users.searchUsers.as_view(), name="searchUsers"),
    path("connectors/", connectors.ConnectorsListView.as_view()),
    path("connectors/<str:cid>/", connectors.ConnectorView.as_view()),
    path("tools/", tools.ToolsListView.as_view()),  # ?investigation=id
    path("tools/<int:tid>/", tools.ToolView.as_view()),
    path(
        "tools/<int:tid>/objects/", tools.ToolObjectsView.as_view()
    ),  # ?parent_id=data_object_id&parent_type=data_object_type
    path(
        "datalinks/", datalinks.DatalinksListView.as_view()
    ),  # ?parent_id=isa_id&parent_type=isa_type
    path("datalinks/<int:did>/", datalinks.DatalinkView.as_view()),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
]
