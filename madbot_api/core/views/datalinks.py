from django.apps import apps
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core import connector as conn
from madbot_api.core import models
from madbot_api.core.serializers import connectors
from madbot_api.core.views.permissions import IsOwner, IsOwnerFilterBackend
from madbot_api.core.views.tools import generate_exception_response_data

from madbot_api.core.connector import (
    DataObjectNotFoundError,
)


class DatalinksListView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.DatalinkSerializer

    def get(self, request):
        """return list of all datalink for the given parent"""
        type_allowed = ["investigation", "study", "assay"]
        if self.request.query_params.get("parent_type"):
            parent_type = str(self.request.query_params.get("parent_type"))
            if parent_type in type_allowed:
                model = apps.get_model("core", parent_type)
                parent = model.objects.get(
                    pk=self.request.query_params.get("parent_id")
                )
                self.check_object_permissions(self.request, parent)
                if parent_type == "investigation":
                    datalink_filter = Q(investigation__id=parent.id)
                elif parent_type == "study":
                    datalink_filter = Q(study__id=parent.id)
                elif parent_type == "assay":
                    datalink_filter = Q(assay__id=parent.id)
                datalinks = models.DataLink.objects.filter(
                    datalink_filter
                ).prefetch_related("tool")
                serialized_datalink = connectors.DatalinkReadSerializer(
                    datalinks, many=True
                )
                return Response(serialized_datalink.data)
            else:
                return Response(
                    "Parent id and type not provided",
                    status=status.HTTP_400_BAD_REQUEST,
                )

    def post(self, request, format=None):
        """create a datalink and link it to an isa object"""
        try:
            type_allowed = ["investigation", "study", "assay", "datalink"]
            parent_type = request.data["parent_type"]
            if parent_type in type_allowed:
                model = apps.get_model("core", parent_type)
                parent = model.objects.get(pk=request.data["parent_id"])
                self.check_object_permissions(self.request, parent)
            else:
                return Response(
                    "Provided parent type error",
                    status=status.HTTP_400_BAD_REQUEST,
                )
            tool = models.Tool.objects.get(pk=request.data["tool"])
            self.check_object_permissions(self.request, tool)
            object_id = request.data["data_object_id"]
            object_type = request.data["data_object_type"]
        except Exception as e:
            return Response(
                "Error Parameters: " + str(e),
                status=status.HTTP_400_BAD_REQUEST,
            )
        connector = conn.set_connector(conn, tool, request.user)
        try:
            dataobject = connector.get_data_object(object_type, object_id)
        except DataObjectNotFoundError as e:
            return Response(
                generate_exception_response_data(
                    type_name=type(e).__name__,
                    connector_name=connector.get_name(),
                    message=str(e),
                ),
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Exception as e:
            return Response(
                generate_exception_response_data(
                    type_name=type(e).__name__,
                    connector_name=connector.get_name(),
                    message=str(e),
                ),
                status=status.HTTP_400_BAD_REQUEST,
            )
        serializer = self.serializer_class.from_dataobject(
            dataobject=dataobject, tool=tool, owner=request.user
        )
        if serializer.is_valid():
            serializer.save(isaobject=parent)
        return Response(serializer.data)


class DatalinkView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.DatalinkSerializer

    def get(self, request, *args, **kwargs):
        """return the specified datalink"""
        datalink = get_object_or_404(models.DataLink, pk=kwargs["did"])
        self.check_object_permissions(self.request, datalink)
        serializer = connectors.DatalinkReadSerializer(datalink)
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        """Modify the specified datalink"""
        datalink = get_object_or_404(models.DataLink, pk=kwargs["did"])
        self.check_object_permissions(self.request, datalink)
        if "name" not in request.data and datalink.name is not None:
            request.data["name"] = datalink.name
        serializer = self.serializer_class(datalink, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        """delete the specified datalink"""
        datalink = self.get_datalink(kwargs)
        datalink.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
