import json

from django.shortcuts import get_object_or_404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core import connector as conn
from madbot_api.core import models
from madbot_api.core.connector import DataObjectNotFoundError
from madbot_api.core.serializers import isa
from madbot_api.core.serializers.connectors import DatalinkSerializer
from madbot_api.core.views.permissions import IsOwner, IsOwnerFilterBackend
from madbot_api.core.views.tools import generate_exception_response_data


class StudiesListView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    filter_backends = IsOwnerFilterBackend
    serializer_class = isa.StudySerializer

    def get(self, request, *args, **kwargs):
        """return a list of studies"""
        investigation = get_object_or_404(models.Investigation, pk=kwargs["iid"])
        queryset = models.Study.objects.filter(parent_investigation=investigation)
        studies = self.filter_backends.filter_queryset(self, self.request, queryset, view=self)
        status = self.request.query_params.get('status')
        if status:
            studies = studies.filter(status=status)
        studies_serializer = isa.StudyReadSerializer(studies, many=True)
        return Response(studies_serializer.data)

    def post(self, request, *args, **kwargs):
        """create a study"""
        serializer = isa.StudySerializer(data=request.data)
        investigation = get_object_or_404(models.Investigation, pk=kwargs["iid"])
        self.check_object_permissions(self.request, investigation)

        if serializer.is_valid() and investigation:
            new_study = serializer.save(
                                        owner=request.user,
                                        parent_investigation=investigation)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudyView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = isa.StudySerializer

    def get(self, request, *args, **kwargs):
        """return the specified study"""
        study = get_object_or_404(models.Study, pk=kwargs["sid"])
        self.check_object_permissions(self.request, study)
        serializer = isa.StudyReadSerializer(study)
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        """
        Modify the specified study
        It can be used for datalink and children creation ("import_from")
        """
        study = get_object_or_404(models.Study, pk=kwargs["sid"])
        self.check_object_permissions(self.request, study)
        if "name" not in request.data and study.name is not None:
            request.data["name"] = study.name
        serializer = isa.StudySerializer(study, data=request.data)
        if self.request.query_params.get("import_from"):
            dry_run = False
            if self.request.query_params.get("dry_run"):
                dry_run = eval(self.request.query_params.get("dry_run").capitalize())
            import_from = self.request.query_params.get("import_from")
            res = json.loads(import_from)
            try:
                tool_id = int(res["tool_id"])
                dataobject_id = res["dataobject_id"]
                dataobject_type = res["dataobject_type"]
                tool = models.Tool.objects.get(pk=tool_id)
                self.check_object_permissions(self.request, tool)
            except Exception as e:
                return Response("Error Parameters: " + str(e), status=status.HTTP_400_BAD_REQUEST)
            connector = conn.set_connector(conn, tool, request.user)
            try:
                data_object = connector.get_data_object(dataobject_type, dataobject_id, depth=1)
            except DataObjectNotFoundError as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=connector.get_name(),
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
            except Exception as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=connector.get_name(),
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
            )
            datalinkserializer = DatalinkSerializer.from_dataobject(dataobject=data_object, tool=tool, owner=request.user)
            if datalinkserializer.is_valid():
                datalink_study = datalinkserializer.data
            assays = []
            for data_object_assay in data_object.children:
                datalink_assay_serializer = DatalinkSerializer.from_dataobject(dataobject=data_object_assay, tool=tool, owner=request.user)
                if datalink_assay_serializer.is_valid():
                    datalink_assay = datalink_assay_serializer.data
                data_assay = {
                "name": data_object_assay.name,
                "description": data_object_assay.description,
                "status": "active",
                "owner": request.user.pk,
                "datalinks": [datalink_assay],
                }
                assay_serializer = isa.AssaySerializer(data=data_assay)
                if assay_serializer.is_valid():
                    assays.append(assay_serializer.data)
                else:
                    return Response(assay_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            request.data["name"] = data_object.name
            request.data["description"] = data_object.description
            request.data["status"] = "active"
            request.data["datalinks"] = [datalink_study]
            request.data["children"] = assays
            study_serializer = isa.StudySerializer(study, data = request.data)
            if study_serializer.is_valid(raise_exception=True):
                if dry_run == False:
                    study_serializer.save(owner=request.user)
                    study_serializer = isa.StudySerializer(instance=study)
                    return Response(study_serializer.data)
                else:
                    return Response(request.data)
            else:
                return Response(study_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        """delete the specified study"""
        study = get_object_or_404(models.Study, pk=kwargs["sid"])
        self.check_object_permissions(self.request, study)
        study.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
