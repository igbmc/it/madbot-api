from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from madbot_api.core import connector as conn
from django.templatetags.static import static


class ConnectorsListView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get(self, request):
        """return a list of connectors"""
        connector_list = []
        type_allowed = ["dataconnector", "isaimporter"]
        if self.request.query_params.get("type"):
            type = str(self.request.query_params.get("type"))
            if type not in type_allowed:
                return Response("Provided type error", status=status.HTTP_400_BAD_REQUEST)
            for connector in conn.get_connectors():
                if connector.get_type() == type or type in connector.get_type():
                    connector_list.append(get_connector_info(connector, request))
        else:
            for connector in conn.get_connectors():
                connector_list.append(get_connector_info(connector, request))
        return Response(connector_list)


class ConnectorView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get(self, request, *args, **kwargs):
        """return the specified connector"""
        try:
            connector_class = conn.get_connector_class(kwargs["cid"])
        except:
            return Response("Connector not found", status=status.HTTP_404_NOT_FOUND)
        return Response(get_connector_info(connector_class, request))


def get_connector_info(connector, request):
    data = {
        "id": connector.__name__,
        "name": connector.get_name(),
        "description": connector.get_description(),
        "icon": request.build_absolute_uri(static(connector.get_logo())),
        "fields": connector.get_connection_param()
    }
    return data