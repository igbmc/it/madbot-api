import json
from urllib.request import urlopen

from madbot_api.core.models import (
    Assay,
    DataLink,
    Investigation,
    Study,
)


def create_investigation_demo(request_user, investigation_id=None):
    ## Get demo data
    url = "https://raw.githubusercontent.com/ISA-tools/ISAdatasets/master/json/BII-I-1/BII-I-1.json"

    # store the response of URL
    response = urlopen(url)

    # storing the JSON response
    # from url in data
    data = json.loads(response.read())

    ## Create data object
    if investigation_id is None:
        investigation = Investigation()
    else:
        investigation = Investigation.objects.get(pk=investigation_id)

    investigation.name = data["title"]
    investigation.description = data["description"]
    investigation.owner = request_user
    investigation.status = "active"
    investigation.save()

    for study_item in data["studies"]:
        study = Study()
        study.name = study_item["title"]
        study.description = study_item["description"]
        study.parent_investigation = investigation
        study.owner = request_user
        study.save()

        for assay_item in study_item["assays"]:
            assay = Assay()
            assay.name = assay_item["measurementType"]["annotationValue"]
            assay.parent_study = study
            assay.owner = request_user
            assay.save()

            for file in assay_item["dataFiles"]:
                dl = DataLink()
                dl.name = file["name"]
                dl.owner = request_user
                dl.isaobject = assay
                dl.save()
    return investigation
