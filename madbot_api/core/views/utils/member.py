from madbot_api.core.models import Member

###############################################################################
# Functions used in members.py
###############################################################################


def check_variable_structure(string_variable):
    """
    The function checks if a string variable has a specific structure with a
    colon separating two parts.

    :param string_variable: The string that represents a variable to check
    :return: a boolean value. If the string_variable has a structure of "x:y"
    where x and y are any strings, the function will return True.
    Otherwise, it will return False.
    """

    if len(string_variable.split(":")) == 2:
        return True
    else:
        return False


def check_and_get_variable_value(value_type, member_value):
    """
    The function `check_and_get_variable_value` checks the value type and
    retrieves members based on the provided member value.

    :param value_type: The value_type parameter is a string that specifies
    the type of value being checked. It can have three possible values:
    "username", "id", or "email"
    :param member_value: The `member_value` parameter is the value that
    you want to check against the specified `value_type`. It could be a
    username, an ID, or an email depending on the value of `value_type`
    :return: a queryset of Member objects that match the specified value_type
    and member_value. If the value_type is not one of "username", "id",
    or "email", the function returns False.
    """

    if value_type == "username":
        members = Member.objects.filter(user__username=member_value)
        return members
    elif value_type == "id":
        members = Member.objects.filter(user=member_value)
        return members
    elif value_type == "email":
        members = Member.objects.filter(user__email=member_value)
        return members
    else:
        return False


def roles_and_descriprions():
    """
    The function returns a dictionary containing roles and
    their corresponding descriptions in an investigation.
    :return: a dictionary containing different roles and their descriptions
    in an investigation.
    """
    dict = [
        {
            "role": "owner",
            "description": "full access to the investigation, "
            "can delete it and can manage members",
        },
        {
            "role": "manager",
            "description": "full access to the investigation, "
            "and can manage members",
        },
        {
            "role": "contributor",
            "description": "full access to the investigation, but cannot delete it",
        },
        {
            "role": "collaborator",
            "description": "can view the investigation, but not modify any content",
        },
    ]
    return dict


def get_role(member):
    """
    The function `get_role` returns the role of a member.

    :param member: The `member` parameter is a Member object.
    :return: a string that represents the role of the member.
    """
    if member.investigation_owner:
        return "owner"
    elif member.investigation_manager:
        return "manager"
    elif member.investigation_contributor:
        return "contributor"
    elif member.investigation_collaborator:
        return "collaborator"
    else:
        return None


def count_owners(investigation):
    """
    The function `count_owners` counts the number of members who are owners of a
    specific investigation.

    :param investigation: The investigation parameter is the investigation object
    that you want to count the owners for
    :return: the count of members who are owners of a specific investigation.
    """
    count = Member.objects.filter(
        investigation=investigation, investigation_owner=True
    ).count()
    return count
