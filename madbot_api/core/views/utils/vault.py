import hvac
import os
from django.conf import settings
from rest_framework.exceptions import NotFound
from hvac.exceptions import InvalidRequest
from hvac.exceptions import Forbidden
from rest_framework.response import Response


class MetaSingleton(type):
    __instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in MetaSingleton.__instances:
            MetaSingleton.__instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return MetaSingleton.__instances[cls]

class Vault(metaclass=MetaSingleton):

    def __init__(self):
        self.vault_client = hvac.Client(url=settings.MADBOT_VAULT_URL)
        self.vault_client.auth.approle.login(
                role_id=settings.MADBOT_VAULT_ROLEID, 
                secret_id=settings.MADBOT_VAULT_SECRETID
                )


    def refresh_authentication(self):
        """
        The function refresh_authentication is used to refresh the authentication credentials.
        """
        self.vault_client.auth.approle.login(
            role_id=settings.MADBOT_VAULT_ROLEID, 
            secret_id=settings.MADBOT_VAULT_SECRETID
            )

    def _read_secret(self, user, object):
        """
        The function reads a secret object for a specific user.
        
        :param user: The user parameter represents the user who is trying to access the secret
        information
        :param object: The "object" parameter refers to the object or resource that the user is trying
        to access or interact with. It could be a file, a database record, an API endpoint, or any other
        entity that the system is designed to manage
        """
        path = os.path.join(str(user.id), str(object.id))
        self.vault_client.kv.default_kv_version = 1
        value = self.vault_client.secrets.kv.v1.read_secret(
            mount_point=settings.MADBOT_VAULT_MOUNT_POINT, path=path
        )
        return value["data"]

    def read_secret(self, user, object):
        """read secret in vault"""
        try:
            return self._read_secret(user, object)
        except Forbidden:
            self.refresh_authentication()
            return self._read_secret(user, object)

    def _write_secret(self, user, tool, data):
        """
        The function writes a secret data for a specific user and tool.
        
        :param user: The user parameter represents the user for whom the secret is being written
        :param tool: The "tool" parameter refers to the name or identifier of the tool or software being
        used
        :param data: The `data` parameter is the secret information that you want to write or store. It
        could be any sensitive information such as passwords, API keys, or any other confidential data
        """
        path = os.path.join(str(user.id), str(tool.id))
        self.vault_client.secrets.kv.v1.create_or_update_secret(
            mount_point=settings.MADBOT_VAULT_MOUNT_POINT, path=path, secret=data
        )

    def write_secret(self, user, tool, data):
        """register secret in vault"""
        try:
            return self._write_secret(user, tool, data)
        except Forbidden:
            self.refresh_authentication()
            return self._write_secret(user, tool, data)

    def _delete_secret(self, user, tool):
        """
        The function deletes a secret associated with a user and a tool.
        
        :param user: The user parameter represents the user who wants to delete a secret. It could be a
        username or any other identifier that uniquely identifies the user
        :param tool: The "tool" parameter is a string that represents the name or identifier of the
        secret tool that the user wants to delete
        """
        path = os.path.join(str(user.id), str(tool.id))
        self.vault_client.secrets.kv.v1.delete_secret(mount_point=settings.MADBOT_VAULT_MOUNT_POINT, path=path)

    def delete_secret(self, user, tool):
        """delete secret in vault"""
        try:
            self._delete_secret(user, tool)
        except Forbidden:
            self.refresh_authentication()
            self._delete_secret(user, tool)
