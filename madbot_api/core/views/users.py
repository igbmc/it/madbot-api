from django.contrib.auth import get_user_model

User = get_user_model()
from rest_framework import generics
from madbot_api.core.serializers.user import UserSerializer

from drf_spectacular.utils import (
    extend_schema
)


@extend_schema(
    description="Get the list of users",
)
class searchUsers(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ["username", "email"]
