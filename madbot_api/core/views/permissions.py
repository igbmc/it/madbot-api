from rest_framework import permissions, filters
from madbot_api.core.models import Member


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsOwnerFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(owner=request.user)


###############################################################################
# Permissions - API
###############################################################################

class IsInvestigationMember(permissions.BasePermission):
    """
    API permission : test if user is a member of the investigation
    """
    message = 'Permission denied, you are not a member of this investigation'
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        return bool(Member.objects.filter(user=request.user, 
                                          investigation=view.kwargs["iid"]))


# The `IsOwner` class is a custom permission class in Django that checks if the
# user making the request is the owner of the investigation.
class IsInvestigationOwner(permissions.BasePermission):
    """
    API permission : test if user is the owner of the invetigation
    """
    message = 'Permission denied. Only Owners and Managers of the ' \
              'investigation are allowed to perform this action'
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        return bool(Member.objects.filter(
                    user=request.user,
                    investigation=view.kwargs["iid"],
                    investigation_owner=True))


# The `IsManager` class is a custom permission class in Django that checks if a
# user is an investigation manager.
class IsInvestigationManager(permissions.BasePermission):
    """
    API permission : test if user is an investigation manager
    """
    message = 'Permission denied. Only Owners and Managers of the ' \
              'investigation are allowed to perform this action.'

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        return bool(
                Member.objects.filter(
                    user=request.user,
                    investigation=view.kwargs["iid"],
                    investigation_manager=True))


# The `IsContributor` class is a custom permission class in Django that checks if
# a user is a contributor to an investigation.
class IsInvestigationContributor(permissions.BasePermission):
    """
    API permission : test if user is an investigation contributor
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        bool(Member.objects.filter(
                    user=request.user,
                    investigation=view.kwargs["iid"],
                    investigation_contributor=True))


# The `IsCollaborator` class is a custom permission class in Django that checks if
# a user is a collaborator in an investigation.
class IsInvestigationCollaborator(permissions.BasePermission):
    """
    API permission : test if user is an investigation contributor
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        bool(Member.objects.filter(
                    user=request.user,
                    investigation=view.kwargs["iid"],
                    investigation_collaborator=True))


def method_permission_classes(classes):
    # Instead of setting the permission_classes property on the function, 
    # like the built-in decorator does, this decorator wraps the call and
    # sets the permission classes on the view instance that is being called.
    # This way, the normal get_permissions() doesn't need any changes,
    # since that simply relies on self.permission_classes.
    def decorator(func):
        def decorated_func(self, *args, **kwargs):
            # To work with request permissions, 
            # we do need to call check_permission() from the decorator,
            # because it's orginally called in initial() so before
            # the permission_classes property is patched.
            self.permission_classes = classes
            # this call is needed for request permissions
            self.check_permissions(self.request)
            return func(self, *args, **kwargs)
        return decorated_func
    return decorator