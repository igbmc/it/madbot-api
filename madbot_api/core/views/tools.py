import logging

from django.shortcuts import get_object_or_404
from madbot_api.core import connector as conn
from madbot_api.core.views.utils.vault import Vault
from django.templatetags.static import static

from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core import connector as conn
from madbot_api.core import models
from madbot_api.core.serializers import connectors
from madbot_api.core.views.permissions import IsOwner

from urllib.parse import urlparse

logger = logging.getLogger(__name__)

from madbot_api.core.connector import (
    AuthenticationError,
    DataObjectNotFoundError,
)


def generate_exception_response_data(type_name, connector_name, message):
    """
    The function `generate_exception_response_data` returns a dictionary containing information
    about an exception, including the type, connector, and message.

    :param type_name: The type of exception that occurred. This could be a specific
    error code or a general category of error
    :param connector_name: The `connector_name` parameter is a string that
    represents the name of the connector or module that is raising the exception. It
    is used to identify the source of the exception
    :param message: The `message` parameter is a string that represents the error
    message or information that you want to include in the response data
    :return: A dictionary is being returned.
    """
    return {
        "type": type_name,
        "connector": connector_name,
        "message": str(message),
    }


class ToolsListView(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = connectors.ToolSerializer

    def get(self, request):
        """return a list of tools"""
        if self.request.query_params.get("investigation_id"):
            investigation_id = self.request.query_params.get("investigation_id")
            tool_list = []
            tools = models.Tool.objects.filter(
                owner=request.user, investigation__pk=int(investigation_id)
            )
            connector_type = self.request.query_params.get("type")
            if connector_type:
                for tool in tools:
                    connector_class = conn.get_connector_class(tool.connector)
                    if (
                        connector_type == connector_class.get_type()
                        or connector_type in connector_class.get_type()
                    ):
                        tool_list.append(tool)
            else:
                tool_list = tools
        else:
            return Response(
                "Investigation id not provided",
                status=status.HTTP_400_BAD_REQUEST,
            )

        data_list = []
        for tool in tool_list:
            data = connectors.ToolSerializer(tool).data
            data["parameters"] = {}
            tool_param_serializer = connectors.ToolParamSerializer(
                tool.get_all_public_param(), many=True
            )
            for param in tool_param_serializer.data:
                data["parameters"].update({param["key"]: param["value"]})
            data_list.append(data)
        return Response(data_list)

    def post(self, request, format=None):
        """create a tool"""

        if self.request.query_params.get("investigation_id"):
            try:
                investigation_id = self.request.query_params.get(
                    "investigation_id"
                )
                tool_data = {
                    "name": request.data["name"],
                    "connector": request.data["connector"],
                    "investigation": investigation_id,
                }
                tool_param_data = request.data["parameters"]
                if "url" in tool_param_data:
                    url_parsed = urlparse(tool_param_data["url"])
                    tool_param_data["url"] = "%s://%s" % (
                        url_parsed.scheme,
                        url_parsed.netloc,
                    )
                connector_class = conn.get_connector_class(
                    tool_data["connector"]
                )
                connector = connector_class(tool_param_data)
            except AuthenticationError as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=tool_data["connector"],
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
            except Exception as e:
                if type(e) == KeyError:
                    return Response(
                        generate_exception_response_data(
                            type_name=type(e).__name__,
                            connector_name=tool_data["connector"],
                            message="Field " + str(e) + " not found",
                        ),
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=tool_data["connector"],
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                "Investigation id not provided",
                status=status.HTTP_400_BAD_REQUEST,
            )


        if not tool_data["name"]:
            tool_data["name"] = connector.get_instance_name()
        serializer_tool = connectors.ToolSerializer(data=tool_data)
        if serializer_tool.is_valid():
            new_tool = serializer_tool.save(owner=request.user)

            public_param = {
                k["id"]: tool_param_data[k["id"]]
                for k in connector.get_connection_param()
                if k["access"] == "public"
            }
            private_param = {
                k["id"]: tool_param_data[k["id"]]
                for k in connector.get_connection_param()
                if k["access"] == "private"
            }
            for key, value in public_param.items():
                serializer_toolparam = connectors.ToolParamSerializer(
                    data={"key": key, "value": value}
                )
                if serializer_toolparam.is_valid():
                    serializer_toolparam.save(tool=new_tool)
                else:
                    del connector
                    return Response(
                        serializer_toolparam.errors,
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            vault=Vault()
            vault.write_secret(request.user, new_tool, private_param)
        else:
            del connector
            return Response(
                serializer_tool.errors, status=status.HTTP_400_BAD_REQUEST
            )
        data = serializer_tool.data
        data["parameters"] = {}
        tool_param_serializer = connectors.ToolParamSerializer(
            new_tool.get_all_public_param(), many=True
        )
        for param in tool_param_serializer.data:
            data["parameters"].update({param["key"]: param["value"]})
        del connector
        return Response(data, status=status.HTTP_201_CREATED)


class ToolView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.ToolSerializer

    def get(self, request, *args, **kwargs):
        """return the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        serializer = connectors.ToolSerializer(tool)
        tool_param_serializer = connectors.ToolParamSerializer(
            tool.get_all_public_param(), many=True
        )
        data = serializer.data
        data["parameters"] = {}
        for param in tool_param_serializer.data:
            data["parameters"].update({param["key"]: param["value"]})
        return Response(data)

    def put(self, request, *args, **kwargs):
        """modify parameters for the specified tool"""
        raise NotImplementedError()

    def delete(self, request, *args, **kwargs):
        """delete the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        vault=Vault()
        vault.delete_secret(request.user, tool)
        tool.delete()
        return Response("Succesfuly deleted", status=status.HTTP_204_NO_CONTENT)


class ToolObjectsView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    serializer_class = connectors.ToolSerializer

    def get(self, request, *args, **kwargs):
        """retrieve a list of objets from the specified tool"""
        tool = get_object_or_404(models.Tool, pk=kwargs["tid"])
        self.check_object_permissions(self.request, tool)
        connector = conn.set_connector(conn, tool, request.user)
        mapping_object_list = {
            "root_name": connector.get_root_name(),
            "objects": [],
        }
        depth = self.request.query_params.get("depth")
        if depth:
            try:
                data_object = connector.get_data_object(
                    self.request.query_params.get("parent_type"),
                    self.request.query_params.get("parent_id"),
                    depth,
                )
            except Exception as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=connector,
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
            data_objects = [data_object]
        else:
            try:
                data_objects = connector.get_data_objects(
                    self.request.query_params.get("parent_type"),
                    self.request.query_params.get("parent_id"),
                )
            except DataObjectNotFoundError as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=connector.get_name(),
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
            except Exception as e:
                return Response(
                    generate_exception_response_data(
                        type_name=type(e).__name__,
                        connector_name=connector.get_name(),
                        message=str(e),
                    ),
                    status=status.HTTP_400_BAD_REQUEST,
                )
        for data in data_objects:
            if data.icon.lower().endswith((".png", ".jpg", ".jpeg", ".svg")):
                data.icon = request.build_absolute_uri(static(data.icon))
            serializer = connectors.DataObjectSerializer(data)
            mapping_object_list["objects"].append(serializer.data)

        return Response(mapping_object_list)
