from django.shortcuts import get_object_or_404
from drf_spectacular.utils import (
    OpenApiExample,
    OpenApiParameter,
    extend_schema,
)
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from madbot_api.core.models import Investigation, MadbotUser, Member
from madbot_api.core.serializers import member
from madbot_api.core.views.permissions import (
    IsInvestigationManager,
    IsInvestigationMember,
    IsInvestigationOwner,
    method_permission_classes,
)
from madbot_api.core.views.utils.member import (
    check_and_get_variable_value,
    check_variable_structure,
    count_owners,
    roles_and_descriprions,
)

#############################################################################
# Investigation members APIs
#############################################################################


class InvestigationMemberView(viewsets.ModelViewSet):
    @extend_schema(
        description="Get the members of and their role in a specific investigation",
        parameters=[
            OpenApiParameter(
                name="iid",
                location=OpenApiParameter.PATH,
                description="Investigation id",
            ),
        ],
    )
    @action(
        detail=True,
        methods=["get"],
    )
    @method_permission_classes([permissions.IsAuthenticated, IsInvestigationMember])
    def get_members_of_investigation(self, request, *args, **kwargs):
        """
        The function retrieves the members associated to a specific
        investigation and returns their serialized data, or returns a 404
        error if the investigation is not found.

        :param iid: The parameter "iid" in the above code refers to the
        investigation ID. It is used to filter the Member objects based on
        the investigation ID
        :return: If the investigation_member exists, the serialized data of
        the investigation_member objects will be returned in the response.
        If the investigation_member does not exist, a response with a message
        "Investigation not Found" and a status code of 404 will be returned.
        """
        investigation_member = Member.objects.filter(investigation=kwargs["iid"])
        if investigation_member.exists():
            serializer = member.InvestigationMemberSerializer(
                investigation_member, many=True
            )
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                {
                    "code": "object_not_found",
                    "message": "Investigation not Found",
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    @extend_schema(
        description="Get the role of a user in a specific"
        " investigation based either on his username,"
        " id or email address",
        parameters=[
            OpenApiParameter(
                name="iid",
                location=OpenApiParameter.PATH,
                description="Investigation id",
            ),
            OpenApiParameter(
                name="user_identifier",
                location=OpenApiParameter.PATH,
                description="Specify the type of value you want to use in"
                " your search (id, username or email)",
                examples=[
                    OpenApiExample("id", value="id:your_user_id"),
                    OpenApiExample("username", value="username:your_username"),
                    OpenApiExample("email", value="email:your_email_address"),
                ],
            ),
        ],
    )
    @action(
        detail=True,
        methods=["get"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
        ]
    )
    def get_member_role(self, request, *args, **kwargs):
        """
        The function retrieves the role of a member in an
        investigation based on their username, ID, or email.

        :param user_identifier: The `user_identifier` parameter is a string
        that contains the identifier of the user. It can be in one of three
        formats: "username", "id", or "email". The format is specified by
        the `value_type` variable
        :return: a Response object. If the investigation member exists and
        is not empty, it returns the serialized data of the investigation
        member.
        """

        if check_variable_structure(kwargs["user_identifier"]):
            member_value = kwargs["user_identifier"].split(":")[1]
            value_type = kwargs["user_identifier"].split(":")[0]
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "invalid parameters: user_identifier",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        if check_and_get_variable_value(value_type, member_value):
            investigation_member = check_and_get_variable_value(
                value_type, member_value
            ).filter(investigation=kwargs["iid"])
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "The type specified is incorrect."
                    " Make sure its either id or username or email",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        if investigation_member.exists():
            if IsInvestigationMember.has_permission(request.user, kwargs["iid"]):
                serializer = member.InvestigationMemberSerializer(
                    investigation_member, many=True
                )
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(
                    {
                        "code": "permision_not_allowed",
                        "message": "Not a member of this investigation",
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {
                    "code": "object_not_found",
                    "message": "Investigation not Found",
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    @extend_schema(
        description="Modify the role of a user in a specific"
        " investigation based either on his username,"
        " id or email address",
        request={
            "multipart/form-data": {
                "type": "object",
                "properties": {
                    "role": {
                        "description": "Member role",
                        "type": "str",
                        "required": "true",
                    },
                },
            },
        },
        parameters=[
            OpenApiParameter(
                name="iid",
                location=OpenApiParameter.PATH,
                description="Investigation id",
            ),
            OpenApiParameter(
                name="user_identifier",
                location=OpenApiParameter.PATH,
                description="Specify the type of value you want to use in"
                " your search (id, username or email)",
                examples=[
                    OpenApiExample("id", value="id:your_user_id"),
                    OpenApiExample("username", value="username:your_username"),
                    OpenApiExample("email", value="email:your_email_address"),
                ],
            ),
        ],
    )
    @action(
        detail=True,
        methods=["put"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
            IsInvestigationOwner | IsInvestigationManager,
        ]
    )
    def update_member_role(self, request, *args, **kwargs):
        """
        The function update a member based on their username, ID, or email.

        :param user_identifier: The `user_identifier` parameter is a string
        that specifies how to identify the member to be updated. It is in the
        format "type:value", where "type" can be "username", "id", or
        "email", and "value" is the corresponding value for that type
        :param role: The `role` parameter is a string representing the
        role of the member (owner, manager, contributor or collaborator)
        :return: a Response object with a message and a status code.
        The specific message and status code depend on the conditions
        met in the function.
        """
        ROLE_LIST = ["owner", "manager", "contributor", "collaborator"]

        if check_variable_structure(kwargs["user_identifier"]):
            member_value = kwargs["user_identifier"].split(":")[1]
            value_type = kwargs["user_identifier"].split(":")[0]
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "invalid parameters: user_identifier",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        if request.data["role"] in ROLE_LIST:
            requested_role = request.data["role"]
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "invalid parameters: role",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        if check_and_get_variable_value(value_type, member_value):
            investigation_member = check_and_get_variable_value(
                value_type, member_value
            ).filter(investigation=kwargs["iid"])
        else:
            return Response(
                {
                    "code": "value_parameter_error",
                    "message": "The type specified is incorrect."
                    " Make sure its either id or username or email",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        if investigation_member.exists():
            if count_owners(kwargs["iid"]) == 1 and investigation_member.filter(
                investigation_owner=True
            ):
                return Response(
                    {
                        "code": "operation_not_allowed",
                        "message": "This member is owner and no other owners exists",
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
            elif (
                investigation_member.filter(investigation_owner=True)
                or request.data["role"] == "owner"
                and Member.objects.get(
                    user=request.user, investigation=kwargs["iid"]
                ).investigation_manager
            ):
                return Response(
                    {
                        "code": "operation_not_allowed",
                        "message": "You can only change to a"
                        " role below or equal to yours.",
                    },
                    status=status.HTTP_401_UNAUTHORIZED,
                )
            else:
                update_role = dict(
                    zip(
                        [
                            "investigation_owner",
                            "investigation_manager",
                            "investigation_contributor",
                            "investigation_collaborator",
                        ],
                        map(
                            lambda role_choice: requested_role == role_choice,
                            ROLE_LIST,
                        ),
                    )
                )
                investigation_member.update(**update_role)
                return Response(
                    {
                        "code": "update_done",
                        "message": "Member role has been updated",
                    },
                    status=status.HTTP_200_OK,
                )
        else:
            return Response(
                {
                    "code": "object_not_found",
                    "message": "Investigation not Found",
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    @extend_schema(
        description="Delete a member",
        parameters=[
            OpenApiParameter(
                name="iid",
                location=OpenApiParameter.PATH,
                description="Investigation id",
            ),
            OpenApiParameter(
                name="user_identifier",
                location=OpenApiParameter.PATH,
                description="Specify the type of value you want to use in"
                " your search (id, username or email)",
                examples=[
                    OpenApiExample("id", value="id:your_user_id"),
                    OpenApiExample("username", value="username:your_username"),
                    OpenApiExample("email", value="email:your_email_address"),
                ],
            ),
        ],
    )
    @action(
        detail=True,
        methods=["delete"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
            IsInvestigationMember,
            IsInvestigationOwner | IsInvestigationManager,
        ]
    )
    def delete_member(self, request, *args, **kwargs):
        """
        The function deletes a member based on their username, ID, or email.

        :param user_identifier: The `user_identifier` parameter is a string
        that specifies how to identify the member to be deleted. It is in the
        format "type:value", where "type" can be "username", "id", or
        "email", and "value" is the corresponding value for that type
        :return: The code is returning a response with a message indicating
        whether the member has been successfully deleted or not. The status
        code of the response is also included.
        """
        if check_variable_structure(kwargs["user_identifier"]):
            member_value = kwargs["user_identifier"].split(":")[1]
            value_type = kwargs["user_identifier"].split(":")[0]
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "invalid parameters: user_identifier",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        if check_and_get_variable_value(value_type, member_value):
            member = check_and_get_variable_value(value_type, member_value).filter(
                investigation=kwargs["iid"]
            )
        else:
            return Response(
                {
                    "code": "value_parameter_error",
                    "message": "The type specified is incorrect."
                    " Make sure its either id or username or email",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        self.check_object_permissions(self.request, member)

        if member.filter(investigation_owner=True) and count_owners(kwargs["iid"]) == 1:
            return Response(
                {
                    "code": "operation_not_allowed",
                    "message": "This member is owner and no other owners exists.",
                },
                status=status.HTTP_401_UNAUTHORIZED,
            )
        elif (
            member.filter(investigation_owner=True)
            and Member.objects.get(
                user=request.user, investigation=kwargs["iid"]
            ).investigation_manager
        ):
            return Response(
                {
                    "code": "operation_not_allowed",
                    "message": "You can only delete a member"
                    " with a role below or equal to yours.",
                },
                status=status.HTTP_401_UNAUTHORIZED,
            )
        else:
            member.delete()
            return Response(
                {"code": "delete_done", "message": "Member has been deleted"},
                status=status.HTTP_200_OK,
            )

    @extend_schema(
        description="Get the investigations related to a specific member",
        parameters=[
            OpenApiParameter(
                name="user_identifier",
                location=OpenApiParameter.PATH,
                description="Specify the type of value you want to use in"
                " your search (id, username or email)",
                examples=[
                    OpenApiExample("id", value="id:your_user_id"),
                    OpenApiExample("username", value="username:your_username"),
                    OpenApiExample("email", value="email:your_email_address"),
                ],
            ),
        ],
    )
    @action(
        detail=True,
        methods=["get"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
        ]
    )
    def get_investigation_of_member(self, request, *args, **kwargs):
        """
        The function retrieves investigations associated with a member
        based on their username, ID, or email.

        :param user_identifier: The `user_identifier` parameter is a string
        that contains the identifier of the user. It is expected to be
        in the format "type:value", where "type" can be "username", "id",
        or "email", and "value" is the actual value of the identifier.
        :return: The code is returning a response object. If the
        investigations_member exists, it returns the serialized data of the
        investigations_member. If the investigations_member does not exist, it
        returns a response with the message "Investigation not Found" and a
        status code of 404.
        """

        if check_variable_structure(kwargs["user_identifier"]):
            member_value = kwargs["user_identifier"].split(":")[1]
            value_type = kwargs["user_identifier"].split(":")[0]
        else:
            return Response(
                {
                    "code": "validation_error",
                    "message": "invalid parameters: user_identifier",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        if check_and_get_variable_value(value_type, member_value):
            investigations_member = check_and_get_variable_value(
                value_type, member_value
            )
        else:
            return Response(
                {
                    "code": "value_parameter_error",
                    "message": "The type specified is incorrect."
                    " Make sure its either id or username or email",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        if investigations_member.exists():
            self.check_object_permissions(self.request, investigations_member)
            serializer = member.InvestigationInformationSerializer(
                investigations_member, many=True
            )

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                {
                    "code": "object_not_found",
                    "message": "No found investigation for this user.",
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    @extend_schema(
        description="Create members in a specific investigation",
        request={
            "multipart/form-data": {
                "type": "object",
                "properties": {
                    "username": {
                        "description": "username",
                        "type": "str",
                        "required": "true",
                    },
                    "role": {
                        "description": "Role of the member",
                        "type": "str",
                        "required": "true",
                    },
                },
            },
        },
        parameters=[
            OpenApiParameter(
                name="iid",
                location=OpenApiParameter.PATH,
                description="Investigation id",
            ),
        ],
    )
    @action(
        detail=True,
        methods=["post"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
            IsInvestigationMember,
            IsInvestigationOwner | IsInvestigationManager,
        ]
    )
    def create_member(self, request, *args, **kwargs):
        """
        The function creates a member for an investigation with a specified
        role and email.

        :return: a Response object with a message indicating that the member
        has been created. The status code of the response is 200, indicating
        a successful request.
        """

        if "username" not in request.data:
            raise ParseError("Empty username")
        if "role" not in request.data:
            raise ParseError("Empty role")

        iid = get_object_or_404(Investigation, id=kwargs["iid"])
        u = get_object_or_404(MadbotUser, username=request.data["username"])

        if not Member.objects.filter(user__username=u, investigation=iid).exists():
            if request.data["role"] == "owner":
                if Member.objects.get(
                    user=request.user, investigation=kwargs["iid"]
                ).investigation_owner:
                    Member.objects.create(
                        user=u,
                        investigation=iid,
                        investigation_owner=True,
                    )
                else:
                    return Response(
                        {
                            "code": "operation_not_allowed",
                            "message": "You can only add new member"
                            " with a role below or equal to yours.",
                        },
                        status=status.HTTP_401_UNAUTHORIZED,
                    )

            elif request.data["role"] == "manager":
                Member.objects.create(
                    user=u,
                    investigation=iid,
                    investigation_manager=True,
                )
            elif request.data["role"] == "contributor":
                Member.objects.create(
                    user=u,
                    investigation=iid,
                    investigation_contributor=True,
                )
            elif request.data["role"] == "collaborator":
                Member.objects.create(
                    user=u,
                    investigation=iid,
                    investigation_collaborator=True,
                )
            else:
                return Response(
                    {
                        "code": "validation_error",
                        "message": "invalid parameters: role ",
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {
                    "code": "member_not_created",
                    "message": u.username
                    + " is already a member of this investigation.",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(
            {"code": "members_created", "message": "Members created."},
            status=status.HTTP_201_CREATED,
        )

    @extend_schema(
        description=" Get the roles and their description",
    )
    @action(
        detail=True,
        methods=["get"],
    )
    @method_permission_classes(
        [
            permissions.IsAuthenticated,
        ]
    )
    def get_roles_information(self, request, *args, **kwargs):
        """
        The function returns a response containing a list of
        roles and their descriptions.

        :return: a response object with the roles information and a status
        code of 200.
        """
        roles = roles_and_descriprions()
        return Response(roles, status=status.HTTP_200_OK)
