from django.contrib import admin

# from django.contrib.admin.options import get_content_type_for_model
# from django.urls import reverse
# from django.utils.html import format_html
# from django.utils.translation import ugettext
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

User = get_user_model()

from madbot_api.core.models import (
    Tool,
    ToolParam,
    DataLink,
    Assay,
    Study,
    Investigation,
    MadbotUser,
    Member
)


# Re-register UserAdmin
# admin.site.unregister(User)
class CustomUserAdmin(UserAdmin):
    model = MadbotUser
    list_display = ['username', 'first_name', 'last_name', 'email']


class ViewOnSiteModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": (
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
            )
        }


class ToolAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


class InvestigationAdmin(admin.ModelAdmin):
    list_display = ["name", "id", "owner"]



class StudyAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class AssayAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]


class DataLinkAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]

class MemberAdmin(admin.ModelAdmin):
    
    list_display = ["user", "investigation", "investigation_owner",
                    "investigation_manager", "investigation_contributor",
                    "investigation_collaborator"]

    search_fields = ['user', 'investigation']


admin.site.register(MadbotUser, CustomUserAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(Investigation, InvestigationAdmin)
admin.site.register(Study, StudyAdmin)
admin.site.register(Assay, AssayAdmin)
admin.site.register(Tool, ToolAdmin)
admin.site.register(ToolParam)
admin.site.register(DataLink)
