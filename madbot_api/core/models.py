import logging
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from madbot_api.core.views.utils.vault import Vault
from django.db.models.signals import post_save
from django.dispatch import receiver

logger = logging.getLogger(__name__)

vault_url = settings.MADBOT_VAULT_URL

class MadbotUser(AbstractUser):
    vault_id = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return str(self.username)


class Tool(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    connector = models.CharField(max_length=100, blank=True, null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    investigation = models.ForeignKey('Investigation', on_delete=models.CASCADE, null=True)

    def get_all_public_param(self):
        """
        The function "get_all_public_param" is used to retrieve all public parameters.
        """
        return ToolParam.objects.filter(tool=self.id)

    def get_public_param(self, key):
        """
        The function `get_public_param` retrieves a public parameter value based on a given key.
        
        :param key: The key parameter is a string that represents the name of the public parameter you
        want to retrieve
        """
        value = ToolParam.objects.values_list("value", flat=True).get(
            key=key, tool_id=str(self.id)
        )
        return value

    def get_private_param(self, key, user):
        """
        This function retrieves a private parameter based on a key and user.
        
        :param key: The `key` parameter is a string that represents the name of the private parameter
        that you want to retrieve
        :param user: The "user" parameter is the user object that represents the user making the
        request. It is used to determine if the user has the necessary permissions to access the private
        parameter
        """
        vault=Vault()
        response = vault.read_secret(user, self)
        return response[key]


class ToolParam(models.Model):
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE, null=True)
    key = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)


class DataLink(models.Model):
    name = models.CharField(max_length=1000, blank=True)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE, blank=True, null=True)
    dataobject_id = models.CharField(max_length=1000, blank=True, null=True)
    dataobject_type = models.CharField(max_length=100, blank=True, null=True)
    remote_url = models.CharField(max_length=1000, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    icon = models.CharField(max_length=10000, blank=True, null=True)
    has_children = models.BooleanField(default=False)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    isaobject = GenericForeignKey()
    
    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]


class ISAObject(models.Model):
    ISA_STATUS = (
        ('pending', 'pending'),
        ('active', 'active'),
        ('archived', 'archived'),
    )
    name = models.CharField(max_length=1000, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=100, blank=False, choices=ISA_STATUS)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True


class Investigation(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='investigation')

class Study(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='study')
    parent_investigation = models.ForeignKey(
        Investigation,
        related_name='children',
        on_delete=models.CASCADE,
        blank=True,
        null=True)


class Assay(ISAObject):
    datalinks = GenericRelation(DataLink, related_query_name='assay')
    parent_study = models.ForeignKey(
        Study,
        related_name='children',
        on_delete=models.CASCADE,
        blank=True, 
        null=True)


class Member(models.Model):
    user = models.ForeignKey(
        MadbotUser,
        on_delete=models.CASCADE)

    investigation = models.ForeignKey(
        Investigation,
        on_delete=models.CASCADE,
        related_name="members",
        related_query_name="member",
        blank=True,
        null=True)

    investigation_owner = models.BooleanField(
        help_text="Owner of the investigation",
        default=False)

    investigation_manager = models.BooleanField(
        help_text="Manager of the investigation",
        default=False)

    investigation_contributor = models.BooleanField(
        help_text="Contributors in the investigation",
        default=False)

    investigation_collaborator = models.BooleanField(
        help_text="collaborator in the investigation",
        default=False)

@receiver(post_save, sender=Investigation)
def create_member(sender, instance, created, **kwargs):
    """
    The function creates a Member object when a new instance is created.
    
    :param sender: The `sender` parameter refers to the model class that
    triggered the signal. In this case, it would be the model class
    Investigation
    :param instance: The `instance` parameter refers to the instance of the
    model that triggered the signal.
    :param created: The `created` parameter is a boolean value that indicates
    whether a new instance of the model has been created or an existing
    instance has been updated. It is `True` if a new instance is created,
    and `False` if an existing instance is updated
    """
    if created:
        Member.objects.create(
            user=MadbotUser.objects.get(username=instance.owner),
            investigation_id=instance.id,
            investigation_owner=True)
