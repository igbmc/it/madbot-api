from django.apps import AppConfig
from madbot_api.core.views.utils.vault import Vault
from requests.exceptions import ConnectionError
from hvac.exceptions import InvalidRequest
from hvac.exceptions import Forbidden

import sys
import logging
from django.conf import settings
logger = logging.getLogger(__name__)


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'madbot_api.core'
    label = 'core'

    def ready(self):
        try:
            Vault()
        except ConnectionError:
            sys.exit("ConnectionError Vault : Failed to establish a new connection to vault URL " +settings.MADBOT_VAULT_URL)
        except InvalidRequest:
            sys.exit("InvalidRequest Vault : The provided values for MADBOT_VAULT_ROLEID or MADBOT_VAULT_SECRETID have been rejected" )
        except Forbidden:
            sys.exit("Forbidden Vault : You do not have access rights to a Vault session" )
