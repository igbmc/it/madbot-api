from madbot_api.core.models import Investigation, Study, Assay, ISAObject,\
      Member
from madbot_api.core.serializers.connectors import DatalinkSerializer
from madbot_api.core.serializers.member import InvestigationMemberSerializer
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer


class IsaObjetcSerializer(serializers.ModelSerializer):
    class Meta:
        model = ISAObject
        fields = '__all__'

class AssaySerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Assay
        fields = ('id', 'name', 'description', 'status', 'owner', 'datalinks', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Assay"


class AssayReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assay
        fields = ('id', 'name', 'description', 'status', 'owner')


class StudySerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    children = AssaySerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Study
        fields = ('id', 'name', 'description', 'status', 'owner', 'datalinks', 'children', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Study"


class StudyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = ('id', 'name', 'description', 'status', 'owner')


class InvestigationSerializer(WritableNestedModelSerializer):
    datalinks = DatalinkSerializer(many=True, required=False)
    children = StudySerializer(many=True, required=False)
    isatype = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Investigation
        fields = ('id', 'name', 'description', 'status', 'owner','datalinks', 'children', 'isatype')
        read_only_fields = ['isatype']

    def get_isatype(self, obj):
        return "Investigation"


class InvestigationReadSerializer(IsaObjetcSerializer):
    members = InvestigationMemberSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Investigation
        fields = IsaObjetcSerializer.Meta.fields
        extra_fields = []
        read_only_fields = []

    def __init__(self, *args, **kwargs):
        authorized_extra_fields = ["members"]
        expand = kwargs.pop('expand', None)
        if expand and expand in authorized_extra_fields:
            self.Meta.extra_fields.append(expand)
            self.Meta.read_only_fields.append(expand)
        super(InvestigationReadSerializer, self).__init__(*args, **kwargs)

class InvestigationInformationSerializer(IsaObjetcSerializer):
    id = serializers.SerializerMethodField(required=False)
    name = serializers.SerializerMethodField(required=False)
    description = serializers.SerializerMethodField(required=False)
    status = serializers.SerializerMethodField(required=False)
    owner = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Investigation
        fields = IsaObjetcSerializer.Meta.fields

    def get_id(self, obj):
        return obj.investigation.id

    def get_name(self, obj):
        return obj.investigation.name

    def get_description(self, obj):
        return obj.investigation.description

    def get_status(self, obj):
        return obj.investigation.status

    def get_owner(self, obj):
        return obj.investigation.owner.username
