from madbot_api.core import models
from rest_framework import serializers

class RecursiveField(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        super(RecursiveField, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tool
        fields = ('id', 'name', 'connector', 'owner', 'investigation')


class ToolParamSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ToolParam
        fields = ('tool', 'key', 'value')


class DatalinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataLink
        fields = ('id', 'name', 'tool', 'dataobject_id', 'dataobject_type', 'remote_url', 'size', 'icon', 'has_children', 'owner')

    @classmethod
    def from_dataobject(cls, dataobject, tool, owner):
        data = {
            "name": dataobject.name,
            "tool": tool.id,
            "dataobject_id": dataobject.id,
            "dataobject_type": dataobject.type,
            "remote_url": dataobject.url,
            "size": dataobject.size,
            "icon": dataobject.icon,
            "has_children": dataobject.has_children,
            "owner": owner.pk,
        }
        serializer = DatalinkSerializer(data=data)
        return serializer

class DatalinkReadSerializer(DatalinkSerializer):
    tool = ToolSerializer(read_only=True)


class DataObjectSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
    description = serializers.CharField()
    type = serializers.CharField()
    url = serializers.CharField()
    size = serializers.FloatField()
    icon = serializers.CharField()
    has_children = serializers.BooleanField()
    linkable = serializers.BooleanField()
    children = RecursiveField(many=True)
