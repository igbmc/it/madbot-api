from madbot_api.core.models import Member
from rest_framework import serializers
from django.db.models import Case, When, Value

from madbot_api.core.views.utils.member import roles_and_descriprions


class InvestigationMemberSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField(required=False)
    first_name = serializers.SerializerMethodField(required=False)
    last_name = serializers.SerializerMethodField(required=False)
    username = serializers.SerializerMethodField(required=False)
    email_adress = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Member
        fields = ("username", "first_name", "last_name", "email_adress", "role")

    def get_username(self, obj):
        return obj.user.username

    def get_first_name(self, obj):
        return obj.user.first_name

    def get_last_name(self, obj):
        return obj.user.last_name

    def get_email_adress(self, obj):
        return obj.user.email

    def get_role(self, obj):
        """
        Get the role of a member in an investigation
        based on their username.

        :param obj: The `obj` parameter is an object that represents a specific
        member in an investigation
        :return: The role of a member in an investigation.
        """

        role = list(
            Member.objects.filter(
                investigation=obj.investigation,
                user__username=obj.user.username,
            )
            .annotate(
                role=Case(
                    When(investigation_owner=True, then=Value("owner")),
                    When(investigation_manager=True, then=Value("manager")),
                    When(
                        investigation_contributor=True,
                        then=Value("contributor"),
                    ),
                    When(
                        investigation_collaborator=True,
                        then=Value("collaborator"),
                    ),
                    default=Value("owner"),
                )
            )
            .values_list("role", flat=True)
        )
        dictRole = roles_and_descriprions()

        return [d for d in dictRole if d["role"] == role[0]][0]
