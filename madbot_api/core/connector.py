import inspect
import json
import logging
import sys
from abc import ABC, abstractmethod
from importlib import import_module
from django.conf import settings

logger = logging.getLogger(__name__)

LIST_STRUCTURE = 0
TREE_STRUCTURE = 1


class DataObject:
    """Constructor method"""

    def __init__(
        self,
        id,
        name,
        type=None,
        url=None,
        description=None,
        size=None,
        icon=None,
        has_children=False,
        linkable=False,
        children=[],
    ):
        self.id = id
        self.name = name
        self.type = type
        self.url = url
        self.description = description
        self.size = size
        self.icon = icon
        self.has_children = has_children
        self.linkable = linkable
        self.children = children


class ToolConnector(ABC):
    """Parent class of all connectors, declares the basic functions of a
    connector."""

    @classmethod
    @abstractmethod
    def get_name(cls):
        """Return the connector name.

        Returns:
            str: Connector name.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_type(cls):
        """Return connector type.

        Returns:
            str: Connector.super()
        """

    @classmethod
    @abstractmethod
    def get_description(cls):
        """Return connector description.

        Returns:
            str: Connector.super()
        """

    @classmethod
    @abstractmethod
    def get_connection_param(cls):
        """Return dictionnary of required field for connection.

        Returns:
            str: {"url": "", "login": "", "password": ""}
        """

    @classmethod
    @abstractmethod
    def get_logo(cls):
        """Return the static path of the connector logo.

        Returns:
            str: Connector logo static path.
        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_color(cls):
        """Return the color of the tool.

        Returns:
            str: Connector color.
        """
        raise NotImplementedError()


class DataConnector(ToolConnector):
    """Parent class for data management connector, declares the minimum
    functions to map a data to madbot.
    """

    @classmethod
    @abstractmethod
    def has_access_url(cls):
        """Return True if the mapping object can be accessed online, through an
        URL.

        Returns:
            bool: True if the mapping object be can be accessed through an URL.
        """
        raise NotImplementedError()

    @classmethod
    def has_mapping_options(cls):
        """Return True if the connector has mapping options.

        Returns:
            bool: True if connector has specific mapping options.
        """
        raise NotImplementedError()

    @abstractmethod
    def get_url_link_to_an_object(self, obj_type, obj_id):
        """Get url link for a given object id and type.

        Arguments:
            obj_type (str): type of the object
            obj_id (int): Id of an object
        Returns:
            str:
            url for a given object
        """
        raise NotImplementedError()

    @abstractmethod
    def get_root_name(self):
        raise NotImplementedError()

    @abstractmethod
    def get_data_objects(self, parent_type, parent_id):
        """Return list of all mappable object from a given parent.
        If no parent then return list of first level mappable object.
        Icons can use fontawesome v5+.

        [
            {
                "name": "",
                "id": "",
                icon:, "",
            },
            {
                "name": "",
                "id": "",
                "icon": "",
            }
        ]

        """
        raise NotImplementedError()

    @abstractmethod
    def get_data_object(self, object_id, object_type):
        """Return the mappable object from a given id object type.
        Icons can use fontawesome v5+.

        [
            {
                "name": "",
                "id": "",
                "icon: "",
            }
        ]

        """
        raise NotImplementedError()


class ISAImporter(DataConnector):
    """Parent class for data management connector, declares the minimum
    functions to map a data to madbot.
    """


# The class `AuthenticationError` is a custom exception class that represents an
# error that occurs during authentication.
class AuthenticationError(Exception):
    def __init__(self, error_message):
        """
        The above function is a constructor that initializes an object with an error
        message and calls the constructor of the parent class.

        :param error_message: The error message is a string that describes the error
        that occurred. It is passed as a parameter to the constructor of the class
        """
        self.message = error_message
        super().__init__(self.message)


# The class `ConnectorUnreachableError` is a custom exception that represents an
# error when a connector is unreachable.
class ConnectorUnreachableError(Exception):
    def __init__(self, error_message):
        self.message = error_message
        super().__init__(self.message)


# The `DataObjectNotFoundError` class is an exception class that represents an error when a data
# object is not found.
class DataObjectNotFoundError(Exception):
    def __init__(self, error_message):
        self.message = error_message
        super().__init__(self.message)


class ToolUnreachableError(Exception):
    def __init__(self, tool):
        self.message = (
            tool.get_name() + " server temporarily unavailable, try again later"
        )
        super().__init__(self.message)


class permissionError(Exception):
    def __init__(self, tool):
        self.message = "you do not have the necessary permission for this \
            action"
        super().__init__(self.message)


class DefaultError(Exception):
    def __init__(self, error_message):
        self.message = error_message
        super().__init__(self.message)


def find_connectors_in_module(module):
    connectors = []
    for name, obj in inspect.getmembers(module):
        if (
            inspect.isclass(obj)
            and issubclass(obj, ToolConnector)
            and obj is not ToolConnector
            and obj is not DataConnector
            and obj is not ISAImporter
        ):
            connectors.append(obj)
        if inspect.ismodule(obj) and module.__name__ in obj.__name__:
            connectors.extend(find_connectors_in_module(obj))

    return connectors


def get_connectors():
    """Retrieves dynamically all implementation of connectors in apps"""
    connectors = []

    for app_module_name in settings.INSTALLED_APPS:
        connectors_module = None
        connectors_module_name = f"{app_module_name}.connectors"
        if connectors_module_name in sys.modules:
            connectors_module = sys.modules[connectors_module_name]
        else:
            try:
                connectors_module = import_module(connectors_module_name)
            except ModuleNotFoundError:
                pass
                # logger.debug(f'No connectors module in {app_module_name}')
        connectors.extend(find_connectors_in_module(connectors_module))

    return connectors


def get_connector_class(connector_name):
    """
    The function `get_connector_class` returns the class object of a connector
    basedon its name.

    :param connector_name: The `connector_name` parameter is a string that
    represents the name of the connector class you want to retrieve
    :return: the class object of the connector with the given name.
    """

    for connector in get_connectors():
        if connector.__name__ == connector_name:
            return connector

    raise Exception("Connector not found")


def get_connector(parameters, tool):
    """
    Return instance of the connector corresponding to the given tool instance
    """
    connector_class = get_connector_class(tool.connector)
    return connector_class(parameters)


def set_connector(self, tool, user):
    """
    The function sets the connector for a given tool and user.
    
    :param tool: The "tool" parameter is the name or identifier of the tool or software that the user
    wants to connect to
    :param user: The "user" parameter refers to the user who is using the tool. It could be the username
    or any other identifier that uniquely identifies the user
    """
    parameters = {}
    connector_class = self.get_connector_class(tool.connector)
    connector_param = connector_class.get_connection_param()
    for param in connector_param:
        if param["access"] == "public":
            parameters[param["id"]] = tool.get_public_param(param["id"])
        elif param["access"] == "private":
            parameters[param["id"]] = tool.get_private_param(
                key=param["id"], user=user
            )
    return self.get_connector(parameters, tool)
