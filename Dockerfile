FROM mambaorg/micromamba:alpine

COPY pyproject.toml .
COPY --chmod=755 entrypoint.sh /opt/

# Install some madbot-api dependencies
RUN micromamba install -y -n base -c conda-forge \
    python=3.10 \
    zeroc-ice=3.6.5 \
    compilers \
    openldap \
    psycopg2 \
    gunicorn gevent && \
    micromamba clean --all --yes

# Install madbot-api from python package
ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN pip install madbot-api[all]==$(grep -m 1 version pyproject.toml | tr -s ' ' | tr -d '"' | tr -d "'" | cut -d' ' -f3) \
    --index-url https://gitlab.com/api/v4/projects/51743040/packages/pypi/simple

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/_entrypoint.sh", "/opt/entrypoint.sh"]
