# Madbot API

## Overview

### Description

API of Madbot, the Metadata And Data Brokering Online Tool

### Language

[![Made with Django](https://img.shields.io/badge/Made%20with-Django-blue)](https://www.djangoproject.com/)
[![Made with Django REST framework](https://img.shields.io/badge/Made%20with-Django%20REST%20framework-blue)](https://www.django-rest-framework.org/)

**Continuous Integration**

[![Pylama](https://img.shields.io/badge/Pylama-coming%20soon-green)](https://pypi.org/project/pylama/)
[![Blake](https://img.shields.io/badge/Blake-coming%20soon-green)](https://black.readthedocs.io/en/stable/)

**Questions**

[:speech_balloon: Ask a question/suggestions or report a bug](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues/new)
[:book: List of questions/suggestions and bugs](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues)
[:book: General questions](mailto:ifb-madbot@groupes.france-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

## Requirements

To run Madbot locally, ensure you have the following tool installed:

- [python](https://www.python.org/)

## Installation procedure for developers

Follow the above steps to set up the development environment for Madbot.

**Clone the Repository :**

```bash
git git@gitlab.com:ifb-elixirfr/madbot/madbot-api.git
ou
git clone https://gitlab.com/ifb-elixirfr/madbot/madbot-api.git

cd madbot-api
```

**Create and activate the conda environment :**

```bash
conda create -p ./env -c conda-forge python=3.10 zeroc-ice=3.6.5

conda activate ./env

pip install -r requirements.txt
```

**Create a configuration file .env :**

```bash
DEBUG=True
ALLOWED_HOSTS=127.0.0.1,localhost
MADBOT_AUTH_BACKEND=JWT
MADBOT_JWT_AUTHORIZED_CLIENT=https://gitlab.com:<APPLICATION ID>
MADBOT_VAULT_URL=http://localhost:8200
MADBOT_VAULT_ROLEID=<VAULT_ROLEID>
MADBOT_VAULT_SECRETID=<VAULT_SECRETID>
```

Remplacez "APPLICATION ID" par l’ID de l’application obtenu sur gitlab.com

**Run migrations and start the server :**

```bash
python manage.py makemigrations core
python manage.py migrate
python manage.py collectstatic
python manage.py runserver
```

## Verification of the dependencies and code quality

Before every push in GitLab, it is recommended to test the following commands:

**Code audit :**

```bash
pylama -i W,E501 --skip "env/lib/*, *migrations*"
```

**Verification of the code quality :**

```bash
black --check .
```

**Dependencies check :**

```bash
pip check
```

Note : In order not to fail at the time of continuous integration, no errors should be detected.

## Developpement Best Practices

### Conventional Commit Messages

We follow the [Conventional Commits](https://www.conventionalcommits.org/) convention for our commit messages. Please adhere to this format for all commits in this project to maintain a coherent and readable code history.

**Format of the commit message :**

```html
<type>(<scope>): <subject>

<body>

<footer>
```

**Example of a commit message :**

```sh
git commit -m "feat: add new API endpoint"
```

**Allowed 'type' values :**

- **build** : Builds - changes that affect the build system or external dependencies
- **chores** : Chores - updating grunt tasks etc; no production code change
- **ci** : Continous Integration - chanhes to our CI configuration files and scripts
- **docs** : Documentation - changes to the documentation
- **feat** : Features - new feature for the user, not a new feature for build script
- **fix** : Bug fixes - bug fix for the user, not a fix to a build script
- **perf** : Performance Improvements - a code change that improves performance
- **refactor** : Code Refactoring - a code change that neither fixes a bug or adds a feature
- **reverts** : Reverts - reverts a previous commit
- **style** : Style - formatting, missing semi colons, etc; no production code change
- **test** : Tests - adding missing tests, refactoring tests; no production code change

**Example 'scope' values :**

- init
- runner
- watcher
- config
- web-server
- proxy
- etc.
The "scope" can be empty (e.g. if the change is a global or difficult to assign to a single component), in which case the parentheses are omitted. In smaller projects such as Karma plugins, the "scope" is empty.

**Message body :**

- Uses the imperative, present tense: “change” not “changed” nor “changes”
- Includes motivation for the change and contrasts with previous behavior

**Message footer :**

- **Referencing issues** : Closed issues should be listed on a separate line in the footer prefixed with "Closes" keyword like this :

```sh
Closes#234
```

or in case of multiple issues :

```sh
Closes#123, #245, #992
```

- **Breaking changes** : a commit that has a footer BREAKING CHANGE:, or appends a ! after the type/scope, introduces a breaking API change (correlating with MAJOR in Semantic Versioning). A BREAKING CHANGE can be part of commits of any type.

**Useful websites**
<https://www.conventionalcommits.org/en/v1.0.0/>
<http://karma-runner.github.io/1.0/dev/git-commit-msg.html>

## Contributors

- [Bouri Laurent](https://gitlab.com/lbouri1) <a itemprop="sameAs" content="https://orcid.org/0000-0002-2297-1559" href="https://orcid.org/0000-0002-2297-1559" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Denecker Thomas](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Messak Imane](https://gitlab.com/imanemessak) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1654-6652" href="https://orcid.org/0000-0002-1654-6652" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Mohamed Anliat](https://gitlab.com/anliatm) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1105-8262" href="https://orcid.org/0000-0002-1105-8262" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Rousseau Baptiste](https://gitlab.com/nobabar) <a itemprop="sameAs" content="https://orcid.org/0009-0002-1723-2732" href="https://orcid.org/0009-0002-1723-2732" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Seiler Julien](https://gitlab.com/julozi) <a itemprop="sameAs" content="https://orcid.org/0000-0002-4549-5188" href="https://orcid.org/0000-0002-4549-5188" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Contribution

Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of conduct

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## Licence

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Madbot API is licensed under a [The 3-Clause BSD License License](https://opensource.org/license/bsd-3-clause/).

## Acknowledgement

- Nadia GOUE and Matéo HIRIART for their contribution to openLink ;
- The working group for its advisory role ;
- All application testers.

## Citation

If you use Matbot API project, please cite us :

IFB-ElixirFr, Matbot API, (2023), GitLab repository, <https://gitlab.com/ifb-elixirfr/madbot/madbot-api.git>