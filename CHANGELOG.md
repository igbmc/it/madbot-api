# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1-dev.7](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.6...v1.0.1-dev.7) (2023-10-31)


### Bug Fixes

* pypi register package id ([bfab433](https://gitlab.com/igbmc/it/madbot-api/commit/bfab433d6a4497c9139489d982b47e6995683c57))

## [1.0.1-dev.6](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.5...v1.0.1-dev.6) (2023-10-31)


### Bug Fixes

* dind variables ([0b48fa3](https://gitlab.com/igbmc/it/madbot-api/commit/0b48fa3f45dc5f48cf52d771c5321b791f4fbbb7))

## [1.0.1-dev.5](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.4...v1.0.1-dev.5) (2023-10-31)


### Bug Fixes

* docker build ([c66ee58](https://gitlab.com/igbmc/it/madbot-api/commit/c66ee5874b2ede1126a1749a7a154156f112de28))

## [1.0.1-dev.4](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.3...v1.0.1-dev.4) (2023-10-31)


### Bug Fixes

* docker job ([d58ac15](https://gitlab.com/igbmc/it/madbot-api/commit/d58ac15814eeb5e5dad5fa1f0534a05a85a2be07))

## [1.0.1-dev.3](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.2...v1.0.1-dev.3) (2023-10-31)


### Bug Fixes

* put dockerfile back in root ([14f4fc0](https://gitlab.com/igbmc/it/madbot-api/commit/14f4fc078107147069ab17ca7440bb35d04543ce))

## [1.0.1-dev.2](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.1-dev.1...v1.0.1-dev.2) (2023-10-31)


### Bug Fixes

* Dockerfile ([d0fc918](https://gitlab.com/igbmc/it/madbot-api/commit/d0fc918c9bd00415f4e8eb986c2841c21ac0dae9))

## [1.0.1-dev.1](https://gitlab.com/igbmc/it/madbot-api/compare/v1.0.0...v1.0.1-dev.1) (2023-10-31)


### Bug Fixes

* fix CI pipeline and prerelease setting ([79b8480](https://gitlab.com/igbmc/it/madbot-api/commit/79b8480beac77637d43a108f7afd4dabd89250f8))

## 1.0.0 (2023-10-31)


### Features

* activate autorelease ([d944251](https://gitlab.com/igbmc/it/madbot-api/commit/d9442512c4b49fb21848fcd64a600966dd4b09d4))
* add investigation to a Tool creation ([f8c8af6](https://gitlab.com/igbmc/it/madbot-api/commit/f8c8af679e1b547264fcbe8d444c7d127ee71440))
* add members permissions for the PUT ([525bc5a](https://gitlab.com/igbmc/it/madbot-api/commit/525bc5a46e037d780d3f194519b08c351d9bdf78))
* add permissions for the delete ([3b19b15](https://gitlab.com/igbmc/it/madbot-api/commit/3b19b158a1308fa20152407a228e899f4e3fdb11))
* add permissions in the creation of a member ([e917f1a](https://gitlab.com/igbmc/it/madbot-api/commit/e917f1ab000390313f48a6dd7d77ee23d2ba65b1))
* add permissions on the GETs members ([5bcb15c](https://gitlab.com/igbmc/it/madbot-api/commit/5bcb15cf2dd32f0bc8d144968dbc71aee999f48a))
* better support dataobject descriptions and links when creating a datalink ([c5cf9a3](https://gitlab.com/igbmc/it/madbot-api/commit/c5cf9a330272d4bfc0334b6686b24b281a113635))
* create a release and CI config files ([6f76428](https://gitlab.com/igbmc/it/madbot-api/commit/6f764285d68250d7db1187c59a7aebc3ca4a08a9))
* create a Swagger api ([4caa885](https://gitlab.com/igbmc/it/madbot-api/commit/4caa8858298bf1b988f107a17da2b5cb1b75c4d9))
* create CI pipeline to check quality ([100c6a2](https://gitlab.com/igbmc/it/madbot-api/commit/100c6a2d128881e8b3a188a710aafef101361dae))
* Create one member at a time ([7aeeaa8](https://gitlab.com/igbmc/it/madbot-api/commit/7aeeaa8399b52d1ebf50c0fa956dc166a0b965f1))
* Define permissions and add them to a custom decorators in the APIs of members management ([af32dfc](https://gitlab.com/igbmc/it/madbot-api/commit/af32dfcf0c9b465c7353200941f075cb0788f26e))
* delete an investigation by only the owners ([ecd8f0c](https://gitlab.com/igbmc/it/madbot-api/commit/ecd8f0c42ffdae81231985c1cc2b79ff75ce5b75))
* generate a demo ISA objects tree ([4edaceb](https://gitlab.com/igbmc/it/madbot-api/commit/4edacebddf6e8dd67bb55fca01ef3c8c74bca51e))
* improve exception error for connector ([0d2c0d9](https://gitlab.com/igbmc/it/madbot-api/commit/0d2c0d92d67deb2c52858736d386c9aa2ff7b702))
* license changed to BS3 ([7dbca7b](https://gitlab.com/igbmc/it/madbot-api/commit/7dbca7b38efa4be5f99158826b2bf630135caf46))
* Manage investigation members ([7a6b47f](https://gitlab.com/igbmc/it/madbot-api/commit/7a6b47fb2c2e6f3bd535ddf8bda8e54fb3492f7f))
* manage POST/PUT/DELETE of a member with a role below or equal to theirs ([00044db](https://gitlab.com/igbmc/it/madbot-api/commit/00044db9542c0efe491cb3a4318ecafd2699df98))
* manage the creation of multiple members ([d1d074f](https://gitlab.com/igbmc/it/madbot-api/commit/d1d074fe56d9f1472d53e81d71e3d534f43bffd1))
* new CI pipeline including better audit, python package release and docker image release ([be2bc50](https://gitlab.com/igbmc/it/madbot-api/commit/be2bc509a004bf88d1b970b9b470ca0c540d093e))
* new CI pipeline including better audit, python package release and docker image release ([dd98790](https://gitlab.com/igbmc/it/madbot-api/commit/dd98790c5541488da787ce66e52efed383e1e9d5))
* only show tools from a given investigation ([0b51191](https://gitlab.com/igbmc/it/madbot-api/commit/0b511911b2f0578387e0d8acac8d60771e6b1ecc))
* prerelease ([335defa](https://gitlab.com/igbmc/it/madbot-api/commit/335defa84f02589743fc9ead067872e5ae87a11f))
* return connectors, tools and dataobject icons image as an url and add support for FontAwesome icons ([9c5ec62](https://gitlab.com/igbmc/it/madbot-api/commit/9c5ec62038e39ee984d8efe43850d0b5ee941a36))
* return the member properties in the investigation API ([d4fde96](https://gitlab.com/igbmc/it/madbot-api/commit/d4fde9609541a5aca1c6c804800afb691661244c))


### Bug Fixes

* "blake" corrected to "black" in ci ([881b01d](https://gitlab.com/igbmc/it/madbot-api/commit/881b01d95acaff2fa48c9016df252c75b1926776))
* add a decoretor for the investigation delete ([94f27e0](https://gitlab.com/igbmc/it/madbot-api/commit/94f27e0f4638b6b787a7e83ded2b02ff1e5bb37f))
* add labguru project url ([d25462f](https://gitlab.com/igbmc/it/madbot-api/commit/d25462f0b6f0c6f18ff7caf6855b41ae77576dc6))
* rename openlink to madbot ([b863b9d](https://gitlab.com/igbmc/it/madbot-api/commit/b863b9dcc76b6aa482dcdcc40c5691b6a9c1cd28))
* **sshfs:** enables navigation in SSHFS subfolders again ([c826af9](https://gitlab.com/igbmc/it/madbot-api/commit/c826af9892e9bb5a80b844c18ee2cd7bc5c616cf))
